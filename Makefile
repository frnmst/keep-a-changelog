#!/usr/bin/env make
#
# Makefile
#

install-dev:
	bundle install

update: install-dev
	bundle update

build:
	bundle exec middleman build --verbose

clean:
	rm -rf build

.PHONY: install-dev update build clean
